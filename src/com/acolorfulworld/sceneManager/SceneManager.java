package com.acolorfulworld.sceneManager;

import java.io.IOException;

import org.andengine.audio.music.Music;
import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import com.acolorfulworld.mainActivity.MainActivity;
import com.acolorfulworld.scenes.CreditsScene;
import com.acolorfulworld.scenes.LevelScene;
import com.acolorfulworld.scenes.LevelSelectionScene;
import com.acolorfulworld.scenes.MenuScene;
import com.acolorfulworld.scenes.SplashScene;

public class SceneManager {
	
	//:::::::::::::::::::::::::::::::::://
	//:::::ENGINE, ACTIVITY, CAMERA::::://
	//:::::::::::::::::::::::::::::::::://
	private MainActivity mainActivity;
	private Engine engine;
	private Camera camera;
	
	//:::::::::::::::::::::::::::::::::://
	//::::::::::::::SCENES:::::::::::::://
	//:::::::::::::::::::::::::::::::::://
	private Scene splashScene;
	private Scene menuScene;
	private Scene levelSelectScene;
	private Scene creditsScene;
	private LevelScene currentScene;
	
	public enum SceneType
	{
		SPLASH,
		MENU,
		LEVEL_SELECTION,
		CREDITS,
		LEVEL_ONE,
		LEVEL_TWO,
		LEVEL_THREE
	}
	
	//:::::::::::::::::::::::::::::::::://
	//:::::::::::::RESSOURCES::::::::::://
	//:::::::::::::::::::::::::::::::::://
	private BitmapTextureAtlas splashBitmapTextureAtlas;
	private TextureRegion splashTextureRegion;
	
	private BitmapTextureAtlas spritesBitmapTextureAtlas;
	private TextureRegion menuBackground;
	private TextureRegion creditsBackground;
	private TextureRegion levelSelectionBackground;
	private TiledTextureRegion menuButtonStartTiledTextureRegion;
	private TiledTextureRegion menuButtonLvlSelectTiledTextureRegion;
	private TiledTextureRegion menuButtonCreditsTiledTextureRegion;
	private TiledTextureRegion menuButtonBackTiledTextureRegion;
	private TiledTextureRegion levelSelectOneTiledTextureRegion;
	private TiledTextureRegion levelSelectTwoTiledTextureRegion;
	private TiledTextureRegion levelSelectThreeTiledTextureRegion;
	private TiledTextureRegion heroTiledTextureRegion;
	private TiledTextureRegion orbTiledTextureRegion;
	
	private BitmapTextureAtlas onScreenControlBitmapTextureAtlas;	
	private ITextureRegion onScreenControlBaseTextureRegion;
	private ITextureRegion onScreenControlKnobTextureRegion;
	
	private Music menuMusic;
	private Music trineFirstMusic;
	private Sound footstepSound;
	
	private ITexture fontTexture;
	private Font mFont;
	
	//:::::::::::::::::::::::::::::::::://
	//::::::::::::::METHODS::::::::::::://
	//:::::::::::::::::::::::::::::::::://
	/**
	 * SceneManager constructor which init the engine, the parent activity, the camera
	 * and the scenes.
	 * 
	 * @param mainActivity
	 * @param engine
	 */
	public SceneManager(MainActivity mainActivity, Engine engine) {
		this.mainActivity = mainActivity;
		this.engine = engine;
		
		this.splashScene = null;
		this.menuScene = null;
		this.levelSelectScene = null;
		this.creditsScene = null;
		this.currentScene = null;
	}

	
	/**
	 * Method loads all of the splash scene resources.
	 */
	public void loadSplashSceneResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		this.splashBitmapTextureAtlas = new BitmapTextureAtlas(this.mainActivity.getTextureManager(), 800, 480, TextureOptions.DEFAULT);
		this.splashTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.splashBitmapTextureAtlas, this.mainActivity, "img_splash.png", 0, 0);
		this.splashBitmapTextureAtlas.load();
	}
	
	
	/**
	 * Method loads all of the resources for the game scenes.
	 */
	public void loadGameSceneResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		this.spritesBitmapTextureAtlas = new BitmapTextureAtlas(this.mainActivity.getTextureManager(), 800, 1852);
		this.menuBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "menu.png", 0, 0);
		this.creditsBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "credits.png", 0, 480);
		this.levelSelectionBackground = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "levelSelect.png", 0, 960);
		this.menuButtonStartTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "menuStartBtn.png", 400, 1440, 1, 2);
		this.menuButtonLvlSelectTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "menuLvlSelectBtn.png", 0, 1440, 1, 2);
		this.menuButtonCreditsTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "menuCreditsBtn.png", 550, 1440, 1, 2);
		this.menuButtonBackTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "menuBackBtn.png", 0, 1550, 1, 2);
		this.levelSelectOneTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "levelSelectOneBtn.png", 150, 1550, 1, 2);
		this.levelSelectTwoTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "levelSelectTwoBtn.png", 350, 1550, 1, 2);
		this.heroTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "hero_steps.png", 550, 1550, 3, 4);
		this.levelSelectThreeTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "levelSelectThreeBtn.png", 0, 1660, 1, 2);
		this.orbTiledTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.spritesBitmapTextureAtlas, this.mainActivity, "yellow_orb.png", 202, 1660, 6, 6);
		
		this.spritesBitmapTextureAtlas.load();
		
		this.onScreenControlBitmapTextureAtlas = new BitmapTextureAtlas(this.mainActivity.getTextureManager(), 256, 128, TextureOptions.BILINEAR);
		this.onScreenControlBaseTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.onScreenControlBitmapTextureAtlas, this.mainActivity, "onscreen_control_base.png", 0, 0);
		this.onScreenControlKnobTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.onScreenControlBitmapTextureAtlas, this.mainActivity, "onscreen_control_knob.png", 128, 0);
		this.onScreenControlBitmapTextureAtlas.load();
		
	    try
	    {
			this.menuMusic = MusicFactory.createMusicFromAsset(this.engine.getMusicManager(), this.mainActivity,"mfx/Trine_Soundtrack_Astral_Academy.ogg");
			this.trineFirstMusic = MusicFactory.createMusicFromAsset(this.engine.getMusicManager(), this.mainActivity,"mfx/Trine_Soundtrack_Fangle_Forest.ogg");
			this.footstepSound = SoundFactory.createSoundFromAsset(this.engine.getSoundManager(), this.mainActivity, "mfx/footstep.ogg");

			this.trineFirstMusic.setVolume(0.1f);
			this.menuMusic.setLooping(true);
			this.menuMusic.setVolume(0.5f);
			this.trineFirstMusic.setLooping(true);
			this.footstepSound.setLooping(true);
			this.footstepSound.setVolume(1f);
	    }
	    catch (IOException e)
	    {
	      e.printStackTrace();
	    }
	    
		FontFactory.setAssetBasePath("font/");
		this.fontTexture = new BitmapTextureAtlas(this.mainActivity.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
		this.mFont = FontFactory.createFromAsset(this.mainActivity.getFontManager(), fontTexture, this.mainActivity.getAssets(), "GOTHIC.TTF", 20, true, android.graphics.Color.WHITE);
		this.mFont.load();	    
	}	
	
	public void setScene(SceneType sceneType) {
		switch (sceneType) {
			case SPLASH:
				break;
			case MENU:
				if (this.menuScene == null) {
					this.createMenuScene();
				}
				else {
					MainActivity.getCamera().setCenter(MainActivity.getCameraWidth() * 0.5f, MainActivity.getCameraHeight() * 0.5f);
					if (!this.menuMusic.isPlaying()) {
						this.menuMusic.seekTo(0);
						this.menuMusic.resume();
					}
					this.engine.setScene(this.menuScene);				
				}
				break;
			case LEVEL_SELECTION:
				if (this.levelSelectScene == null) {
					this.createLevelSelectionScene();
				}
				else {
					/*MainActivity.getCamera().setCenter(MainActivity.getCameraWidth() * 0.5f, MainActivity.getCameraHeight() * 0.5f);
					if (!this.menuMusic.isPlaying()) {
						this.menuMusic.seekTo(0);
						this.menuMusic.resume();
					}*/
					this.engine.setScene(this.levelSelectScene);				
				}
				break;
			case CREDITS:
				if (this.creditsScene == null) {
					this.createCreditsScene();
				}
				else {
					//MainActivity.getCamera().setCenter(MainActivity.getCameraWidth() * 0.5f, MainActivity.getCameraHeight() * 0.5f);
					this.engine.setScene(this.creditsScene);	
				}
				break;
			case LEVEL_ONE:
				this.createLevelScene("tmx/level1.tmx");
				this.menuMusic.pause();
				this.trineFirstMusic.seekTo(0);
				this.trineFirstMusic.resume();
				break;
			case LEVEL_TWO:
				this.createLevelScene("tmx/level2.tmx");
				this.menuMusic.pause();
				this.trineFirstMusic.seekTo(0);
				this.trineFirstMusic.resume();
				break;
			case LEVEL_THREE:
				this.createLevelScene("tmx/level3.tmx");
				this.menuMusic.pause();
				this.trineFirstMusic.seekTo(0);
				this.trineFirstMusic.resume();
				break;
		}
	}
	
	/**
	 * Method creates the Splash Scene
	 * @return splashScene 
	 */
	public Scene createSplashScene() {
		this.splashScene = new SplashScene(this);
		return this.splashScene;
	}
	
	public Scene createMenuScene()
	{
		this.menuScene = new MenuScene(this);
		this.engine.setScene(this.menuScene);
		this.menuMusic.play();
		return this.menuScene;
	}
	
	public Scene createLevelScene(String tiledMapLevel) {
		this.currentScene = new LevelScene(this, tiledMapLevel);
		this.engine.setScene(this.currentScene);
		return this.currentScene;
	}
	
	public Scene createLevelSelectionScene() {
		this.levelSelectScene = new LevelSelectionScene(this);
		this.engine.setScene(this.levelSelectScene);
		return this.levelSelectScene;
	}
	
	public Scene createCreditsScene()
	{
		this.creditsScene = new CreditsScene(this);
		this.engine.setScene(this.creditsScene);
		return this.creditsScene;
	}
	
	public MainActivity getMainActivity() {
		return mainActivity;
	}

	public Engine getEngine() {
		return engine;
	}

	public Camera getCamera() {
		return camera;
	}

	public BitmapTextureAtlas getSplashBitmapTextureAtlas() {
		return splashBitmapTextureAtlas;
	}

	public TextureRegion getSplashTextureRegion() {
		return splashTextureRegion;
	}

	public TiledTextureRegion getMenuButtonStartTiledTextureRegion() {
		return menuButtonStartTiledTextureRegion;
	}

	public TiledTextureRegion getHeroTiledTextureRegion() {
		return heroTiledTextureRegion;
	}

	public ITextureRegion getOnScreenControlBaseTextureRegion() {
		return onScreenControlBaseTextureRegion;
	}

	public ITextureRegion getOnScreenControlKnobTextureRegion() {
		return onScreenControlKnobTextureRegion;
	}
	
	public TextureRegion getMenuBackground() {
		return menuBackground;
	}
	
	public TiledTextureRegion getMenuButtonLvlSelectTiledTextureRegion() {
		return menuButtonLvlSelectTiledTextureRegion;
	}

	public TiledTextureRegion getMenuButtonCreditsTiledTextureRegion() {
		return menuButtonCreditsTiledTextureRegion;
	}

	public Music getMenuMusic() {
		return menuMusic;
	}
	
	public Music getTrineFirstMusic() {
		return trineFirstMusic;
	}
	
	public Sound getFootstepSound() {
		return footstepSound;
	}

	public Font getmFont() {
		return mFont;
	}

	public TextureRegion getCreditsBackground() {
		return creditsBackground;
	}

	public TiledTextureRegion getMenuButtonBackTiledTextureRegion() {
		return menuButtonBackTiledTextureRegion;
	}
	
	public TextureRegion getLevelSelectionBackground() {
		return levelSelectionBackground;
	}

	public TiledTextureRegion getLevelSelectOneTiledTextureRegion() {
		return levelSelectOneTiledTextureRegion;
	}

	public TiledTextureRegion getLevelSelectTwoTiledTextureRegion() {
		return levelSelectTwoTiledTextureRegion;
	}

	public TiledTextureRegion getLevelSelectThreeTiledTextureRegion() {
		return levelSelectThreeTiledTextureRegion;
	}

	public TiledTextureRegion getOrbTiledTextureRegion() {
		return orbTiledTextureRegion;
	}
	
}
