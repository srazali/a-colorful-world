package com.acolorfulworld.inGameCharacter;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.acolorfulworld.mainActivity.MainActivity;
import com.acolorfulworld.sceneManager.SceneManager;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class InGameOrb {
	
	private SceneManager sceneManager;
	
	private AnimatedSprite orbAnimatedSprite;
	private Body orbBody;

	public InGameOrb(SceneManager sceneManager, float pX, float pY,
			ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		this.sceneManager = sceneManager;
		
		// Create orb sprite and body
		this.orbAnimatedSprite = new AnimatedSprite(pX, pY, 32, 32, pTiledTextureRegion, this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.orbBody = PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), orbAnimatedSprite, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 1f, true));
        this.orbBody.setUserData("orb");
        this.orbBody.setActive(true);
        this.orbBody.setAwake(true);
        this.orbBody.setSleepingAllowed(false);
        
		MainActivity.getPhysicsWorld().registerPhysicsConnector(new PhysicsConnector(this.orbAnimatedSprite, this.orbBody, true, true){
            @Override
            public void onUpdate(float pSecondsElapsed){
                    super.onUpdate(pSecondsElapsed);
            }
        });
	}
	
	public AnimatedSprite getOrbAnimatedSprite() {
		return orbAnimatedSprite;
	}

	public Body getOrbBody() {
		return orbBody;
	}
	
}
