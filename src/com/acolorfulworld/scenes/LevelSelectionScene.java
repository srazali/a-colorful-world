package com.acolorfulworld.scenes;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;

import com.acolorfulworld.sceneManager.SceneManager;
import com.acolorfulworld.sceneManager.SceneManager.SceneType;

public class LevelSelectionScene extends Scene {
	
	private SceneManager sceneManager;
	private TiledSprite btnBack;
	private TiledSprite btnLevelOne;
	private TiledSprite btnLevelTwo;
	private TiledSprite btnLevelThree;
	
	public LevelSelectionScene(SceneManager sceneManager) {
		this.sceneManager = sceneManager;
		this.addBackgroundSprite();
		this.addBtnBackSprite();
		this.addBtnLevelOneSprite();
		this.addBtnLevelTwoSprite();
		this.addBtnLevelThreeSprite();
	}
	
	private void addBackgroundSprite() {
		Sprite background = new Sprite(0, 0, this.sceneManager.getLevelSelectionBackground(), this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.attachChild(background);
	}

	public void addBtnBackSprite() {
		this.btnBack = new TiledSprite(0, 0, 150, 55, this.sceneManager.getMenuButtonBackTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.MENU);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		this.btnBack.setPosition(595f, 375f);
		
		this.attachChild(this.btnBack);
		this.registerTouchArea(this.btnBack);
	}
	
	public void addBtnLevelOneSprite() {
		this.btnLevelOne = new TiledSprite(0, 0, 200, 55, this.sceneManager.getLevelSelectOneTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.LEVEL_ONE);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		this.btnLevelOne.setPosition(100f, 200f);
		
		this.attachChild(this.btnLevelOne);
		this.registerTouchArea(this.btnLevelOne);
	}
	
	public void addBtnLevelTwoSprite() {
		this.btnLevelTwo = new TiledSprite(0, 0, 200, 55, this.sceneManager.getLevelSelectTwoTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.LEVEL_TWO);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		this.btnLevelTwo.setPosition(300f, 200f);
		
		this.attachChild(this.btnLevelTwo);
		this.registerTouchArea(this.btnLevelTwo);
	}
	
	public void addBtnLevelThreeSprite() {
		this.btnLevelThree = new TiledSprite(0, 0, 200, 55, this.sceneManager.getLevelSelectThreeTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.LEVEL_THREE);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		this.btnLevelThree.setPosition(500f, 200f);
		
		this.attachChild(this.btnLevelThree);
		this.registerTouchArea(this.btnLevelThree);
	}
	
}
