package com.acolorfulworld.mainActivity;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.ui.activity.BaseGameActivity;
import com.acolorfulworld.sceneManager.SceneManager;
import com.acolorfulworld.sceneManager.SceneManager.SceneType;
import com.badlogic.gdx.math.Vector2;

public class MainActivity extends BaseGameActivity {
	
	private final static int CAMERA_WIDTH = 800;
	private final static int CAMERA_HEIGHT = 480;
	private static BoundCamera camera;
	
	private SceneManager sceneManager;
	
	private static PhysicsWorld physicsWorld;
	
	@Override
	public EngineOptions onCreateEngineOptions() {
		camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), camera);
		engineOptions.getAudioOptions().setNeedsMusic(true);
		engineOptions.getAudioOptions().setNeedsSound(true);
		return engineOptions;
	}
	
	@Override
	public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception {		
		sceneManager = new SceneManager(this, mEngine);
		sceneManager.loadSplashSceneResources();		
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}
	
	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
		pOnCreateSceneCallback.onCreateSceneFinished(sceneManager.createSplashScene());
	}
	
	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		mEngine.registerUpdateHandler(new TimerHandler(1f, new ITimerCallback() 
		{
			public void onTimePassed(final TimerHandler pTimerHandler) 
			{
				mEngine.unregisterUpdateHandler(pTimerHandler);
				sceneManager.loadGameSceneResources();
				sceneManager.setScene(SceneType.MENU);
				
				// the register handler and the physicsWorld are used for the in game world
				mEngine.registerUpdateHandler(new FPSLogger());
				physicsWorld = new FixedStepPhysicsWorld(50, 1, new Vector2(0, 0), false, 8, 1);
			}
		}));
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
	
	
	//:::::::::::::::::::::::::::::::::://
	//::::::::::::::GETTERS::::::::::::://
	//:::::::::::::::::::::::::::::::::://	
	public static BoundCamera getCamera() {
		return camera;
	}
	
	public static PhysicsWorld getPhysicsWorld() {
		return physicsWorld;
	}

	public static int getCameraWidth() {
		return CAMERA_WIDTH;
	}

	public static int getCameraHeight() {
		return CAMERA_HEIGHT;
	}

	public SceneManager getSceneManager() {
		return sceneManager;
	}
	
}
