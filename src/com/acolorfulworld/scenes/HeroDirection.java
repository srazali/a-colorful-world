package com.acolorfulworld.scenes;

public enum HeroDirection {
	NONE,
    UP,
    DOWN,
    LEFT,
    RIGHT
}
