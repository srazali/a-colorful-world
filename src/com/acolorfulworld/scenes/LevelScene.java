package com.acolorfulworld.scenes;

import java.util.ArrayList;
import java.util.Iterator;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl;
import org.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.andengine.engine.camera.hud.controls.AnalogOnScreenControl.IAnalogOnScreenControlListener;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.shape.RectangularShape;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXObject;
import org.andengine.extension.tmx.TMXObjectGroup;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.util.debug.Debug;
import android.opengl.GLES20;
import android.widget.Toast;

import com.acolorfulworld.inGameCharacter.InGameHero;
import com.acolorfulworld.inGameCharacter.InGameOrb;
import com.acolorfulworld.mainActivity.MainActivity;
import com.acolorfulworld.sceneManager.SceneManager;
import com.acolorfulworld.sceneManager.SceneManager.SceneType;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class LevelScene extends Scene {
	
	private String TMX_TLIED_MAP_NAME;
	
	private SceneManager sceneManager;
	
	// Hero data
	private InGameHero hero;
	private InGameOrb orb;
	private HeroDirection playerDirection = HeroDirection.NONE;
	
	// Map data
	private Body exit;
	private TMXTiledMap mTMXTiledMap;

	// Garbage collector data
	private ArrayList<AnimatedSprite> dAnimatedSprite = new ArrayList<AnimatedSprite>();
	private ArrayList<RectangularShape> dRectangularShape = new ArrayList<RectangularShape>();
	
	private boolean isSoundPlaying;

	public LevelScene(SceneManager sceneManager, String tiledMapLevel) {
		this.sceneManager = sceneManager;
		this.TMX_TLIED_MAP_NAME = tiledMapLevel;
		this.isSoundPlaying = false;
		
		try {
			final TMXLoader tmxLoader = new TMXLoader(this.sceneManager.getMainActivity().getAssets(), this.sceneManager.getEngine().getTextureManager(), TextureOptions.NEAREST, this.sceneManager.getMainActivity().getVertexBufferObjectManager());
			this.mTMXTiledMap = tmxLoader.loadFromAsset(TMX_TLIED_MAP_NAME);
		} catch (final TMXLoadException e) {
			Debug.e(e);
		}
		
		this.addNonObjectLayersToScene();
		this.addBounds(this.mTMXTiledMap.getTMXLayers().get(0).getWidth(), this.mTMXTiledMap.getTMXLayers().get(0).getHeight());
		this.createUnwalkableObjects();
		this.createExitObject();

		this.orb = new InGameOrb(sceneManager, this.getOrbXPosition(), this.getOrbYPosition(), this.sceneManager.getOrbTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.attachChild(this.orb.getOrbAnimatedSprite());
		this.orb.getOrbAnimatedSprite().animate(100, true);
		this.dAnimatedSprite.add(this.orb.getOrbAnimatedSprite());
		
		this.openingAnimation();
	}
	
	public void startLevel() {
		LevelScene.this.sceneManager.getMainActivity().runOnUiThread(new Runnable() {
    		@Override
    		public void run() {
    			Toast.makeText(LevelScene.this.sceneManager.getMainActivity(), "This labyrinth seems complicated... I have to find the orb first!", Toast.LENGTH_LONG).show();
    		}
		});
		this.hero = new InGameHero(sceneManager, "Oxo", this.getStartXPosition(), this.getStartYPosition(), this.sceneManager.getHeroTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.attachChild(this.hero.getHeroAnimatedSprite());
		this.dAnimatedSprite.add(this.hero.getHeroAnimatedSprite());
		
		final AnalogOnScreenControl analogOnScreenControl = new AnalogOnScreenControl(MainActivity.getCameraWidth() - this.sceneManager.getOnScreenControlBaseTextureRegion().getWidth()-80, MainActivity.getCameraHeight() -this.sceneManager.getOnScreenControlBaseTextureRegion().getHeight()-20, MainActivity.getCamera(), this.sceneManager.getOnScreenControlBaseTextureRegion(), this.sceneManager.getOnScreenControlKnobTextureRegion(), 0.1f, 200, this.sceneManager.getMainActivity().getVertexBufferObjectManager(), new IAnalogOnScreenControlListener() {
			
			public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl, final float pValueX, final float pValueY) {
				if (pValueX != 0 || pValueY != 0) {
					if (!LevelScene.this.isSoundPlaying) {
						LevelScene.this.sceneManager.getFootstepSound().play();
						LevelScene.this.isSoundPlaying = true;
					}
					if ((Math.abs(pValueY) >= Math.abs(pValueX)) && (pValueY < 0)) {
						if (playerDirection != HeroDirection.UP) {
							hero.getHeroAnimatedSprite().animate(new long[]{100, 200, 200}, 0, 2, true);
	                        playerDirection = HeroDirection.UP;
	                        hero.getHeroBody().setLinearVelocity(0, -3);
	                    }
		            }
					else if ((Math.abs(pValueY) > Math.abs(pValueX)) && (pValueY > 0)) {
		                // Down
		                if (playerDirection != HeroDirection.DOWN) {
		                	hero.getHeroAnimatedSprite().animate(new long[]{100, 200, 200}, 6, 8, true);
		                    playerDirection = HeroDirection.DOWN;
		                    hero.getHeroBody().setLinearVelocity(0, 3);
		                }
		            }
					else if ((Math.abs(pValueX) >= Math.abs(pValueY)) && (pValueX < 0)) {
		                // Left
		                if (playerDirection != HeroDirection.LEFT) {
		                	hero.getHeroAnimatedSprite().animate(new long[]{100, 200, 200}, 9, 11, true);
		                    playerDirection = HeroDirection.LEFT;
		                    hero.getHeroBody().setLinearVelocity(-3, 0);
		                }
		            }
					else if ((Math.abs(pValueX) > Math.abs(pValueY)) && (pValueX > 0)) {
		                // Right
		                if (playerDirection != HeroDirection.RIGHT) {
		                	hero.getHeroAnimatedSprite().animate(new long[]{100, 200, 200}, 3, 5, true);
		                    playerDirection = HeroDirection.RIGHT;
		                    hero.getHeroBody().setLinearVelocity(3, 0);
		                }
		            }
				}
				else {
	                if (hero.getHeroAnimatedSprite().isAnimationRunning()) {
	                	hero.getHeroAnimatedSprite().stopAnimation();
	                    playerDirection = HeroDirection.NONE;
	                    hero.getHeroBody().setLinearVelocity(0, 0);
	                    if (LevelScene.this.isSoundPlaying) {
	                    	LevelScene.this.sceneManager.getFootstepSound().stop();
	                    	LevelScene.this.isSoundPlaying = false;
	                    }
	                }
	            }
			}
			
			public void onControlClick(final AnalogOnScreenControl pAnalogOnScreenControl) {
				// TODO
			}
		});
		
		analogOnScreenControl.getControlBase().setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
		analogOnScreenControl.getControlBase().setAlpha(0.5f);
		analogOnScreenControl.getControlBase().setScaleCenter(0, 128);
		analogOnScreenControl.getControlBase().setScale(1.25f);
		analogOnScreenControl.getControlKnob().setScale(1.25f);
		analogOnScreenControl.refreshControlKnobPosition();
        
		this.setChildScene(analogOnScreenControl);
		this.registerUpdateHandler(MainActivity.getPhysicsWorld());
		
		// Set contact listener to know when the user is at the exit or not.
		MainActivity.getPhysicsWorld().setContactListener(new ContactListener() {
			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}
			
			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}
			
			@Override
			public void endContact(Contact contact) {
			}
			
			@Override
			public void beginContact(Contact contact) {
				String b = (String) contact.getFixtureA().getBody().getUserData();
		        String a = (String) contact.getFixtureB().getBody().getUserData();
		        if (a != null && b != null) {
			        if ((a.equals("hero") && b.equals("exit")) || (a.equals("exit") && b.equals("hero")))
			        {
			        	if (LevelScene.this.hero.hasOrb()) {
			        		LevelScene.this.myGarbageCollection();
			        		LevelScene.this.sceneManager.setScene(SceneType.MENU);
			        	}
			        	else {
			        		LevelScene.this.sceneManager.getMainActivity().runOnUiThread(new Runnable() {
				        		@Override
				        		public void run() {
				        			Toast.makeText(LevelScene.this.sceneManager.getMainActivity(), "I must get the orb back! I do not want to be invisible forever?...", Toast.LENGTH_LONG).show();
				        		}
			        		});
			        	}
			        }
			        if ((a.equals("hero") && b.equals("orb")) || (a.equals("orb") && b.equals("hero"))) {
			        	if (!LevelScene.this.hero.hasOrb()) {
				        	LevelScene.this.hero.setHasOrb(true);
				        	LevelScene.this.detachChild(LevelScene.this.orb.getOrbAnimatedSprite());
				        	LevelScene.this.sceneManager.getMainActivity().runOnUiThread(new Runnable() {
				        		@Override
				        		public void run() {
				        			Toast.makeText(LevelScene.this.sceneManager.getMainActivity(), "Yes, the orb, I have it! Now I need to find the exit...", Toast.LENGTH_LONG).show();
				        		}
			        		});
			        	}
			        }
		        }
			}
		});
		
	}

	/**
	 *  Add the non-object layers to the scene.
	 */
    public void addNonObjectLayersToScene()
    {
    	for (int i = 0; i < this.mTMXTiledMap.getTMXLayers().size(); i++) {
	        TMXLayer layer = this.mTMXTiledMap.getTMXLayers().get(i);
	        if (!layer.getTMXLayerProperties().containsTMXProperty("walls", "true"))
	        	this.attachChild(layer);
    	}
    }
    
    /**
	 * Create a rectangle for each walls on the TMXTiledMap.
	 * 
	 * @param mTMXTiledMap
	 */
	public void createUnwalkableObjects() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("wall", "true")) {
                // This is our "wall" layer. Create the boxes from it
                for (final TMXObject object : group.getTMXObjects()) {
                       final Rectangle rectangle = new Rectangle(object.getX(), object.getY(),object.getWidth(), object.getHeight(), this.sceneManager.getEngine().getVertexBufferObjectManager());
                       final FixtureDef boxFixtureDef = PhysicsFactory.createFixtureDef(0, 0, 1f);
                       PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), rectangle, BodyType.StaticBody, boxFixtureDef);
                       rectangle.setVisible(false);
                       this.attachChild(rectangle);
                       this.dRectangularShape.add(rectangle);
                }
            }
		}
	}
	
	public void createExitObject() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("end", "true")) {
                for (final TMXObject object : group.getTMXObjects()) {
                       final Rectangle rectangle = new Rectangle(object.getX(), object.getY(),object.getWidth()*0.5f, object.getHeight()*0.5f, this.sceneManager.getEngine().getVertexBufferObjectManager());
                       final FixtureDef boxFixtureDef = PhysicsFactory.createFixtureDef(0, 0, 1f, true);
                       this.exit = PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), rectangle, BodyType.DynamicBody, boxFixtureDef);
                       this.exit.setUserData("exit");
                       rectangle.setVisible(false);
                       this.attachChild(rectangle);
                       this.dRectangularShape.add(rectangle);
                }
            }
		}
	}
	
	public void addBounds(float width, float height){
        final Rectangle bottom = new Rectangle(0, height - 2, width, 2, this.sceneManager.getMainActivity().getVertexBufferObjectManager());
        bottom.setVisible(false);
        final Rectangle top = new Rectangle(0, 0, width, 2, this.sceneManager.getMainActivity().getVertexBufferObjectManager());
        top.setVisible(false);
        final Rectangle left = new Rectangle(0, 0, 2, height, this.sceneManager.getMainActivity().getVertexBufferObjectManager());
        left.setVisible(false);
        final Rectangle right = new Rectangle(width - 2, 0, 2, height, this.sceneManager.getMainActivity().getVertexBufferObjectManager());
        right.setVisible(false);

        final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0, 0, 1f);
        PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), bottom, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), top, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), left, BodyType.StaticBody, wallFixtureDef);
        PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), right, BodyType.StaticBody, wallFixtureDef);

        this.attachChild(bottom);
        this.attachChild(top);
        this.attachChild(left);
        this.attachChild(right);        
        this.dRectangularShape.add(bottom);
        this.dRectangularShape.add(top);
        this.dRectangularShape.add(left);
        this.dRectangularShape.add(right);
	}
	
	public int getStartXPosition() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("start", "true")) {
                for (final TMXObject object : group.getTMXObjects()) {
                       return object.getX();
                }
            }
		}
		return 0;
	}
    
	public int getStartYPosition() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("start", "true")) {
                for (final TMXObject object : group.getTMXObjects()) {
                       return object.getY();
                }
            }
		}
		return 0;
	}
	
	public int getEndXPosition() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("end", "true")) {
                for (final TMXObject object : group.getTMXObjects()) {
                       return object.getX();
                }
            }
		}
		return 0;
	}
    
	public int getEndYPosition() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("end", "true")) {
                for (final TMXObject object : group.getTMXObjects()) {
                	return object.getY();
                }
            }
		}
		return 0;
	}
	
	public int getOrbXPosition() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("orb", "true")) {
                for (final TMXObject object : group.getTMXObjects()) {
                    return object.getX();
                }
            }
		}
		return 0;
	}
	
	public int getOrbYPosition() {
		for (final TMXObjectGroup group: this.mTMXTiledMap.getTMXObjectGroups()) {
            if(group.getTMXObjectGroupProperties().containsTMXProperty("orb", "true")) {
                for (final TMXObject object : group.getTMXObjects()) {
                    return object.getY();
                }
            }
		}
		return 0;
	}
	
	public void openingAnimation() {
		Entity e = new Entity();
	    e.setPosition(this.getStartXPosition(), this.getStartYPosition());
	    MainActivity.getCamera().setChaseEntity(e);
	           
	    final MoveModifier modifier = new MoveModifier(4f, e.getX(), this.getEndXPosition(), e.getY(), this.getEndYPosition())
	    {
	        @Override
	        protected void onModifierFinished(IEntity pItem)
	        {
	            super.onModifierFinished(pItem);
	            MainActivity.getCamera().setChaseEntity(null);
	            LevelScene.this.startLevel();
	        }
	    };
	    
	    e.registerEntityModifier(modifier);
	    this.attachChild(e);
	}
	
	private void myGarbageCollection() {
        Iterator<Body> allMyBodies = MainActivity.getPhysicsWorld().getBodies();
        while(allMyBodies.hasNext())
        {
			try {
				final Body myCurrentBody = allMyBodies.next();
				this.sceneManager.getMainActivity().runOnUpdateThread(new Runnable(){
					@Override
					public void run() {
						MainActivity.getPhysicsWorld().destroyBody(myCurrentBody);                
					}
				});
			} catch (Exception e) {
				Debug.d(e.getMessage());
			}
        }
 
        Iterator<Joint> allMyJoints = MainActivity.getPhysicsWorld().getJoints();
        while(allMyJoints.hasNext())
        {
	        try {
				final Joint myCurrentJoint = allMyJoints.next();
				this.sceneManager.getMainActivity().runOnUpdateThread(new Runnable(){
					@Override
					public void run() {
						MainActivity.getPhysicsWorld().destroyJoint(myCurrentJoint);                
					}
				});
	        } catch (Exception e) {
	            Debug.d(e.getMessage());
	        }
        }
        // check if the ArrayList contains anything
        if(dAnimatedSprite.size()>0){
            for(int i=0; i < dAnimatedSprite.size(); i++){
                this.detachChild(dAnimatedSprite.get(i));
            }
        }
        // unload the contents of the ArrayList
        dAnimatedSprite.clear();
               
		// check if the ArrayList contains anything
        if(dRectangularShape.size()>0){
            for(int i=0; i < dRectangularShape.size(); i++){
                this.detachChild(dRectangularShape.get(i));
            }
        }
        // unload the contents of the ArrayList
        dRectangularShape.clear();
                       
        this.clearChildScene();
        this.detachChildren();
        this.reset();
        this.detachSelf();
        MainActivity.getPhysicsWorld().clearForces();
        MainActivity.getPhysicsWorld().clearPhysicsConnectors();
        MainActivity.getPhysicsWorld().reset();
        MainActivity.getCamera().reset();
        this.hero = null;
        this.orb = null;
        MainActivity.getCamera().setChaseEntity(null);
        MainActivity.getCamera().setCenter(MainActivity.getCameraWidth() * 0.5f, MainActivity.getCameraHeight() * 0.5f);
        this.sceneManager.getTrineFirstMusic().pause();
        this.sceneManager.getFootstepSound().pause();
        System.gc();
	}

}
