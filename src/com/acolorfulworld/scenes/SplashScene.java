package com.acolorfulworld.scenes;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;

import com.acolorfulworld.mainActivity.MainActivity;
import com.acolorfulworld.sceneManager.SceneManager;


public class SplashScene extends Scene {

	public SplashScene(SceneManager sceneManager) {
		addSplashSprite(sceneManager);
	}
	
	private void addSplashSprite(SceneManager sceneManager)
	{
		Sprite splash = new Sprite(0, 0, sceneManager.getSplashTextureRegion(), sceneManager.getMainActivity().getVertexBufferObjectManager());
		splash.setPosition((MainActivity.getCameraWidth() - splash.getWidth()) * 0.5f, (MainActivity.getCameraHeight() - splash.getHeight()) * 0.5f);
		this.attachChild(splash);
	}

}
