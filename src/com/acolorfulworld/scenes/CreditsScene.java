package com.acolorfulworld.scenes;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;

import com.acolorfulworld.sceneManager.SceneManager;
import com.acolorfulworld.sceneManager.SceneManager.SceneType;

public class CreditsScene extends Scene {
	
	private SceneManager sceneManager;
	private TiledSprite btnBack;
	
	public CreditsScene(SceneManager sceneManager) {
		this.sceneManager = sceneManager;
		this.addBackgroundSprite();
		this.addBtnBackSprite();
		
		Text txt = new Text(330, 75, this.sceneManager.getmFont(), 
				"This game has been created thanks to the\n" +
				"AndEngine library which is free and can\n" +
				"be downloaded from git on this url:\n" +
				"github.com/nicolasgramlich/AndEngine.\n\n" +
				"Graphisme: PhotoshopCS6.\n\n" +
				"Music: Trine, www.freesfx.co.uk\n" +
				"\t\tAudacity.\n\n" +
				"Development: Eclipse, Android, AndEngine\n" +
				"\t\tGit, BitBucket, Tiled.", this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.attachChild(txt);
	}
	
	private void addBackgroundSprite() {
		Sprite background = new Sprite(0, 0, this.sceneManager.getCreditsBackground(), this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.attachChild(background);
	}
	
	public void addBtnBackSprite() {
		this.btnBack = new TiledSprite(0, 0, 150, 55, this.sceneManager.getMenuButtonBackTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.MENU);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		this.btnBack.setPosition(595f, 375f);
		
		this.attachChild(this.btnBack);
		this.registerTouchArea(this.btnBack);
	}

}
