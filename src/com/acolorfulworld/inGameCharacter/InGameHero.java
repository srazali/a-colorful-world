package com.acolorfulworld.inGameCharacter;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.acolorfulworld.mainActivity.MainActivity;
import com.acolorfulworld.sceneManager.SceneManager;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class InGameHero {
	
	private String heroName;
	private boolean hasOrb;
	private SceneManager sceneManager;
	
	private AnimatedSprite heroAnimatedSprite;
	private Body heroBody;

	public InGameHero(SceneManager sceneManager, String heroName, float pX, float pY,
			ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		this.heroName = heroName;
		this.hasOrb = false;
		this.sceneManager = sceneManager;
		
		// Create hero sprite and body
		this.heroAnimatedSprite = new AnimatedSprite(pX, pY, 20, 20, pTiledTextureRegion, this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.heroBody = PhysicsFactory.createBoxBody(MainActivity.getPhysicsWorld(), heroAnimatedSprite, BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 1f));
        this.heroBody.setUserData("hero");
        
        // Set camera to chase hero
        MainActivity.getCamera().setChaseEntity(this.heroAnimatedSprite);
        
        // Link hero animated sprite to hero body
		MainActivity.getPhysicsWorld().registerPhysicsConnector(new PhysicsConnector(this.heroAnimatedSprite, this.heroBody, true, true){
            @Override
            public void onUpdate(float pSecondsElapsed){
                    super.onUpdate(pSecondsElapsed);
                    MainActivity.getCamera().updateChaseEntity();
            }
        });
	}

	public String getHeroName() {
		return heroName;
	}
	
	public AnimatedSprite getHeroAnimatedSprite() {
		return heroAnimatedSprite;
	}

	public Body getHeroBody() {
		return heroBody;
	}
	
	public boolean hasOrb() {
		return this.hasOrb;
	}

	public void setHasOrb(boolean b) {
		this.hasOrb = b;
		
	}
	
}
