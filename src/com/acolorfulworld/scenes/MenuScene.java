package com.acolorfulworld.scenes;

import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.input.touch.TouchEvent;

import com.acolorfulworld.mainActivity.MainActivity;
import com.acolorfulworld.sceneManager.SceneManager;
import com.acolorfulworld.sceneManager.SceneManager.SceneType;


public class MenuScene extends Scene {
	
	private SceneManager sceneManager;
	private TiledSprite btnStart;
	private TiledSprite btnLvlSelect;
	private TiledSprite btnCredtits;

	public MenuScene(SceneManager sceneManager) {
		this.sceneManager = sceneManager;
		addBackgroundSprite();
		this.addBtnStartSprite();
		this.addBtnLvlSelectSprite();
		this.addBtnCreditsSprite();
	}

	private void addBackgroundSprite() {
		Sprite background = new Sprite(0, 0, this.sceneManager.getMenuBackground(), this.sceneManager.getMainActivity().getVertexBufferObjectManager());
		this.attachChild(background);
	}
	
	public void addBtnStartSprite() {
		this.btnStart = new TiledSprite(0, 0, 150, 55, this.sceneManager.getMenuButtonStartTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.LEVEL_ONE);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		this.btnStart.setPosition((MainActivity.getCameraWidth() - this.btnStart.getWidth()) * 0.5f, 180f);
		
		this.attachChild(this.btnStart);
		this.registerTouchArea(this.btnStart);
	}
	
	public void addBtnLvlSelectSprite() {
		this.btnLvlSelect = new TiledSprite(0, 0, 400, 55, this.sceneManager.getMenuButtonLvlSelectTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.LEVEL_SELECTION);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		btnLvlSelect.setPosition((MainActivity.getCameraWidth() - btnLvlSelect.getWidth()) * 0.5f, 270f);
		
		this.attachChild(btnLvlSelect);
		this.registerTouchArea(btnLvlSelect);
	}
	
	public void addBtnCreditsSprite() {
		this.btnCredtits = new TiledSprite(0, 0, 250, 55, this.sceneManager.getMenuButtonCreditsTiledTextureRegion(), this.sceneManager.getMainActivity().getVertexBufferObjectManager()) {
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
					this.setCurrentTileIndex(1);
				}
				else if(pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
					this.setCurrentTileIndex(0);
					sceneManager.setScene(SceneType.CREDITS);
				}
				else if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_MOVE) {
					this.setCurrentTileIndex(0);
				}
				return true;
			}
		};
		btnCredtits.setPosition((MainActivity.getCameraWidth() - btnCredtits.getWidth()) * 0.5f, 355f);
		
		this.attachChild(btnCredtits);
		this.registerTouchArea(btnCredtits);
	}
	
	public TiledSprite getBtnStart() {
		return this.btnStart;
	}

}
